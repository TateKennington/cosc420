for i in {1..9}
do
	mkdir learning-0.$i
	mkdir learning-0."$i"5
	echo "Testing alpha = 0.$i + 5"
	for run in {1..10}
	do
		echo "Run $run"
		java Main -l 0.$i >> learning-0.$i/$run.out
		java Main -l 0."$i"5 >> learning-0."$i"5/$run.out
	done
done
