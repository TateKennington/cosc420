/*
 * Cosc420 Assignment 1
 * Author : Tate Kennington
 * Student ID: 5925152
*/

import java.util.Scanner;
import java.io.FileReader;
import java.util.Arrays;

public class Main{

	/*
 	 * Program entry point 
 	*/
	public static void main(String[] args){
		
		Scanner in, teach, param;									/*Input file Scanners*/
		int input, hidden, output, batchSize = 1, epochInterval = 100, convergenceEpochs = 1000000;	/*Network parameters*/
		double learning, momentum, errorCriteria, convergenceThreshold = 0;				/*Network parameters*/
		boolean t_flag = false, w_flag = false;								/*Command line flags*/

		//Open all input files
		try{	
			in = new Scanner(new FileReader("in.txt"));
			teach = new Scanner(new FileReader("teach.txt"));
			param = new Scanner(new FileReader("param.txt"));
		} catch(Exception e){
			System.out.println("Error: Couldn't open file");
			return;
		}
	
		//Parse all compulsory parameters
		input = param.nextInt();
		hidden = param.nextInt();
		output = param.nextInt();
		learning = param.nextDouble();
		momentum = param.nextDouble();
		errorCriteria = param.nextDouble();

		//Parse optional parameters
		if(param.hasNextInt()){
			batchSize = param.nextInt();	
		}
		if(param.hasNextInt()){
			epochInterval = param.nextInt();
		}
		if(param.hasNextDouble()){
			convergenceThreshold = param.nextDouble();
		}
		if(param.hasNextInt()){
			convergenceEpochs = param.nextInt();
		}

		//Parse command line flags
		for(int i = 0; i<args.length; i++){
			if(args[i].equals("-l")){
				learning = Float.parseFloat(args[i+1]);
			}
			if(args[i].equals("-m")){
				momentum = Float.parseFloat(args[i+1]);
			}
			if(args[i].equals("-t")){
				t_flag = true;
			}
			if(args[i].equals("-w")){
				w_flag = true;
			}
		}

		//Count the number of patterns
		int patterns = 0;
		String patternText = "";
		while(in.hasNextLine()){
			patternText += in.nextLine()+"\n";
			patterns++;
		}

		//Parse the input patterns
		double[][] inputPatterns = new double[patterns][input];
		Scanner patternIn = new Scanner(patternText);
		for(int i = 0; i<patterns; i++){
			for(int j = 0; j<input; j++){
				inputPatterns[i][j] = patternIn.nextDouble();
			}
		}

		//Parse the teaching patterns
		double[][] teachingPatterns = new double[patterns][output];
		for(int i = 0; i<patterns; i++){
			for(int j = 0; j<output; j++){
				teachingPatterns[i][j] = teach.nextDouble();
			}
		}

		//Train the neural network
		NeuralNetwork nn = new NeuralNetwork(input, hidden, output, learning, momentum, batchSize, epochInterval, convergenceThreshold, convergenceEpochs);
		nn.train(inputPatterns, teachingPatterns, errorCriteria);
		System.out.println("Finished Training\n");
		
		//Do any output if necessary
		if(t_flag)nn.run(inputPatterns);
		if(w_flag)nn.printStats();
	}

}
