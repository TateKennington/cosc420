for i in {1..9}
do
	mkdir momentum-0.$i
	echo "Testing Momentum 0.$i"
	for run in {1..10}
	do
		echo "Run $run"
		java Main -m 0.$i >> momentum-0.$i/$run.out
	done
done
