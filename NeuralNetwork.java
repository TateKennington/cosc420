/*
 * Cosc420 Assignment 1
 * Author: Tate Kennington
 * Student ID : 5925152
*/


/*
 * Class representing a neural network
*/
public class NeuralNetwork{

	double learningRate = 0.05; 					/*Delta rule learning rate*/
	double momentumConstant = 0.9; 					/*Momentum coefficient*/
	final double epsilon = 0.01; 					/*Error value*/
	double convergenceThreshold = 0.00001;				/*Convergence radius*/
	double convergenceEpochs = 1000;				/*Convergence timelimit*/
	int maxAttempts = 10;						/*Number or retries*/

	int input;							/*Number of input neurons*/
	int hidden;							/*Number of hidden neurons*/
	int output;							/*Number of output neurons*/
	int batchSize;							/*Number of patterns in a batch*/
	int epochInterval;						/*Number of epochs between printouts*/
	
	double convergenceValue;					/*Current potential convergence value*/
	int attempts = 0;						/*Current number of attempts*/
	int currentEpochs = 0;						/*Current number of epichs spent near convergence value*/
	
	Neuron[][] neurons;						/*Collection of neurons*/
	Layer[] layers;							/*Collection of trainable weights*/

	/*
	 * Sigmoid activation function
	*/
	public static double sigmoid(double activation){
		return 1/(1+Math.exp(-activation));
	}
	
	/*
	 * Sigmoid derivative function
	*/
	public static double sigmoidD(double activation){
		return sigmoid(activation)*(1-sigmoid(activation)); 
	}

	/*
	 * Constructor
	*/
	public NeuralNetwork(int _input, int _hidden, int _output, double _learningRate, double _momentumConstant, int _batchSize, int _epochInterval, double _convergenceThreshold, int _convergenceEpochs){
		//Initialize member variables
		input = _input; hidden = _hidden; output = _output;
		learningRate = _learningRate; momentumConstant = _momentumConstant;
		batchSize = _batchSize;
		epochInterval = _epochInterval;
		convergenceThreshold = _convergenceThreshold;

		//Initialize neurons
		neurons = new Neuron[3][];
		neurons[0] = new Neuron[input];		
		neurons[1] = new Neuron[hidden];
		neurons[2] = new Neuron[output];
		
		for(int i = 0; i<input; i++){
			neurons[0][i] = new Neuron();
		}
		for(int i = 0; i<hidden; i++){
			neurons[1][i] = new Neuron();
		}	
		for(int i = 0; i<output; i++){
			neurons[2][i] = new Neuron();
		}

		//Initialize layers
		layers = new Layer[2];

		layers[0] = new Layer(input, hidden, neurons[0], neurons[1]);
		layers[1] = new Layer(hidden, output, neurons[1], neurons[2]);
	}		

	/*
	 * Resets all neurons to initial state
	*/
	public void resetNeurons(){
		for(int i = 0; i<input; i++){
			neurons[0][i].activation = 0;
			neurons[0][i].delta = 0;
			neurons[0][i].netInput = 0;
		}
		for(int i = 0; i<hidden; i++){
			neurons[1][i].activation = 0;
			neurons[1][i].delta = 0;
			neurons[1][i].netInput = 0;
		}	
		for(int i = 0; i<output; i++){
			neurons[2][i].activation = 0;
			neurons[2][i].netInput = 0;
		}
	}

	/*
	 * Calculates population error
	*/
	public double calculateError(double[][] testInput, double[][] testOutput){
		double res = 0; /*Output population error*/
		
		//For every pattern
		for(int i = 0; i<testInput.length; i++){
			//Calculate the network output
			resetNeurons();
			feedForward(testInput[i]);
			
			//Accumulate the pattern error
			for(int j = 0; j<output; j++){
				res += (testOutput[i][j] - neurons[2][j].activation)*(testOutput[i][j] - neurons[2][j].activation);
			}
		}
		
		//Normalize population error
		res /= output*testOutput.length;
	
		return res;
	}

	/*
	 * Trains the network to within a given error tolerance
	*/
	public void train(double[][] testInput, double[][] testOutput, double errorThreshold){
		double error=0;  /*Current network error*/
		int epoch = 0;   /*Current training epoch*/
		
		java.util.Random r = new java.util.Random();
		boolean[] seen = new boolean[testInput.length];
		
		//While the error is greater than the threshold
		do{

			for(int i = 0; i<testInput.length; i++){
				seen[i] = false;	
			}

			//Train the network on each pattern
			for(int i = 0; i<testInput.length; i+=batchSize){
				
				//Batch patterns at random
				for(int j = 0; j<batchSize && i+j<testInput.length; j++){
					resetNeurons();
					int curr = 0;
					do{
						curr = (int)(r.nextFloat()*testInput.length);
					} while(seen[curr]);
					seen[curr] = true;
					feedForward(testInput[curr]);
					accumulateDelta(testOutput[curr]);
				}
				
				//Do backpropagation
				backProp();
			}

			//Update epoch and calculate new error
			epoch++;
			error = calculateError(testInput, testOutput);
			
			//Check if we have left the radius of convergence
			if(Math.abs(convergenceValue - error) > convergenceThreshold){
				//Reset convergence trackers
				convergenceValue = error;
				currentEpochs = 0;	
			}
			else if(convergenceThreshold > 0){
				
				//Check for convergence
				currentEpochs++;

				//If we have converged restart training
				if(currentEpochs > convergenceEpochs){
					attempts++;
					if(attempts>=maxAttempts) return;
					System.out.println("Restarting, Epoch: "+epoch);
					convergenceValue = 0;
					currentEpochs = 0;
					epoch = 0;
					resetNeurons();
					for(int i = 0; i<layers.length; i++){
						layers[i].reset();
					}
				}
			}

			//Output stats every epochInterval
			if(epoch%epochInterval == 0){
				System.out.println("Epoch: "+epoch+", Error: "+error);
			}
		} while(error > errorThreshold);

	}

	/*
 	 * Add the current pattern delta onto the output delta
 	*/ 
	public void accumulateDelta(double[] outputPattern){
		//Set output layer delta
		for(int i = 0; i<output; i++){
			neurons[2][i].delta += (outputPattern[i] - neurons[2][i].activation)*sigmoidD(neurons[2][i].netInput);
		}
	}

	/*
	 * Calculate network output on a set on input patterns
	*/
	public void run(double[][] input){
		//For every input pattern
		for(int i = 0; i<input.length; i++){
			//Reset the network and feed forward the pattern
			resetNeurons();
			feedForward(input[i]);
			
			//Output results as input:output pair
			System.out.println("Network input : output");
			System.out.print(input[i][0]);
			for(int j = 1; j<input[i].length; j++){
				System.out.print(" "+input[i][j]);
			}
			System.out.print(" :");
			for(int j = 0; j<output; j++){
				System.out.print(" "+neurons[2][j].activation);
			}
			System.out.println();
			printActivation();
			System.out.println();
		}
	}

	/*
	 * Prints the current activation of all neurons in the network
	*/
	public void printActivation(){
		System.out.println("Network Activation:");
		for(int i = 0; i<3; i++){
			for(int j = 0; j<neurons[i].length; j++){
				System.out.print(neurons[i][j].activation+" ");
			}
			System.out.println();
		}
	}

	/*
	 * Prints the current delta value of all neurons in the network
	*/
	public void printDelta(){
		System.out.println("Network Delta:");
		for(int i = 0; i<3; i++){
			for(int j = 0; j<neurons[i].length; j++){
				System.out.print(neurons[i][j].delta+" ");
			}
			System.out.println();
		}
	}

	/*
	 * Prints the current weights configuration of the network
	*/
	public void printStats(){
		System.out.println("Network Weights");
		System.out.println("Layer 1: Input:"+input+", Output: "+hidden);
		layers[0].printWeights();
		System.out.println("Layer 2");
		layers[1].printWeights();
	}

	/*
	 * Run back propagation on the current network state, against a given pattern
	*/
	void backProp(){
		for(int i = 0; i<output; i++){
			neurons[2][i].delta /= batchSize;
		}

		//Calculate hidden layer delta
		layers[1].backProp();
		layers[0].backProp();
	}

	/*
	 * Calculate network activation for a given input pattern
	*/
	void feedForward(double[] inputPattern){
		//Fix input neuron activations
		for(int i = 0; i<input; i++){
			neurons[0][i].activation = inputPattern[i];
		}
	
		//Calculate net input
		layers[0].propagate();

		//Calculate activation
		for(int i = 0; i<hidden; i++){
			neurons[1][i].activation = sigmoid(neurons[1][i].netInput);
			
			//Round values close to 0 or 1
			if(neurons[1][i].activation < epsilon) neurons[1][i].activation = 0;
			if(1-neurons[1][i].activation < epsilon) neurons[1][i].activation = 1;
		}

		//Calculate net input
		layers[1].propagate();

		//Calculate activation
		for(int i = 0; i<output; i++){
			neurons[2][i].activation = sigmoid(neurons[2][i].netInput);
			
			//Round values close to 0 or 1
			if(neurons[2][i].activation < epsilon) neurons[2][i].activation = 0;
			if(1-neurons[2][i].activation < epsilon) neurons[2][i].activation = 1;
		}

	}
    
	/*
	 * Class representing a layer of trainable weights between neurons
	*/
	class Layer{
		
		double bias[];					/*Bias weights*/
		double prevBiasChange[];			/*Previous iteration bias weight changes*/
		double weights[][];				/*Layer weights*/
		double prevWeightChange[][];			/*Previous interation weight changes*/
		Neuron[] inputNeurons;				/*References to input neurons*/
		Neuron[] outputNeurons;				/*References to ouput neurons*/
		int input;					/*Number of input neurons*/
		int output;					/*Number of output neurons*/

		/*
		 * Constructor
		*/
		public Layer(int _input, int _output, Neuron[] _inputNeurons, Neuron[] _outputNeurons){
			//Initialize member variables
			input = _input;
			output = _output;
			weights = new double[input][output];
			prevWeightChange = new double[input][output];
			bias = new double [output];
			prevBiasChange = new double [output];
			inputNeurons = _inputNeurons;
			outputNeurons = _outputNeurons;

			//Initialize layer weights
			java.util.Random r = new java.util.Random();
			for(int i = 0; i<input; i++){
				for(int j = 0; j<output; j++){
					weights[i][j] = r.nextDouble() * 0.6 - 0.3;
					prevWeightChange[i][j] = 0;
				}
			}
			for(int i = 0; i<output; i++){
				bias[i] = r.nextDouble() * 0.6 - 0.3;
				prevBiasChange[i] = 0;
			}
		}

		public void reset(){
			//Initialize layer weights
			java.util.Random r = new java.util.Random();
			for(int i = 0; i<input; i++){
				for(int j = 0; j<output; j++){
					weights[i][j] = r.nextDouble() * 0.6 - 0.3;
					prevWeightChange[i][j] = 0;
				}
			}
			for(int i = 0; i<output; i++){
				bias[i] = r.nextDouble() * 0.6 - 0.3;
				prevBiasChange[i] = 0;
			}
		}

		/*
		 * Backpropagate error from the output neurons to the input neurons
		*/
		public void backProp(){
			//Calculate deltas
			//For every input neuron
			for(int i = 0; i<input; i++){
			
				inputNeurons[i].delta = 0;				
	
				//Accumulate neuron delta
				for(int j = 0; j<output; j++){
					inputNeurons[i].delta += outputNeurons[j].delta*weights[i][j];
				}

				inputNeurons[i].delta *= sigmoidD(inputNeurons[i].netInput);
			}

			//Adjust weights
			//For every output neuron
			for(int j = 0; j<output; j++){
				
				if(Math.abs(outputNeurons[j].delta)<epsilon){
					prevBiasChange[j] = 0;
					for(int i = 0; i<input; i++){
						prevWeightChange[i][j] = 0;
					}
					outputNeurons[j].delta = 0;
					continue;
				}

				//Adjust bias weights
				bias[j] += learningRate*outputNeurons[j].delta + momentumConstant*prevBiasChange[j];
				prevBiasChange[j] = learningRate*outputNeurons[j].delta + momentumConstant*prevBiasChange[j];
	
				//Adjust incoming weights
				for(int i = 0; i<input; i++){
					weights[i][j] += learningRate*inputNeurons[i].activation*outputNeurons[j].delta + momentumConstant*prevWeightChange[i][j];
					prevWeightChange[i][j] = learningRate*inputNeurons[i].activation*outputNeurons[j].delta + momentumConstant*prevWeightChange[i][j];
				}
			}

			//Reset output neuron deltas
			for(int i = 0; i<output; i++){
				outputNeurons[i].delta = 0;
			}
		}

		/*
		 * Feed forward activation from the input neurons to the output neurons
		*/
		public void propagate(){
			//For every output neuron
			for(int j = 0; j<output; j++){

				//Accumulate net input to that neuron
				outputNeurons[j].netInput += bias[j];
				for(int i = 0; i<input; i++){
					outputNeurons[j].netInput += inputNeurons[i].activation * weights[i][j];
				}
			}
		}
	
		/*
		 * Prints layer weights
		*/
		public void printWeights(){
			//Print neuron weights
			for(int i = 0; i<input; i++){
				for(int j = 0; j<output; j++){
					System.out.println("i:"+i+", j:"+j+", "+weights[i][j]);
				}
			}

			//Print bias weights
			System.out.print("Bias:");
			for(int i = 0; i<output; i++){
				System.out.print(" "+bias[i]);
			}
			System.out.println();
		}

	}

	/*
	 * Class representing neurons
	*/
	class Neuron{

		public double netInput;		/*Neurons net input*/
		public double activation;	/*Neurons activation*/
		public double delta;		/*Neurons delta value*/

		/*
		 * Default Constructor
		*/
		public Neuron(){
			netInput = 0;
			activation = 0;
			delta = 0;
		}

	}

}
