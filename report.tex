\documentclass{article}

\usepackage{graphicx}

\title{COSC420 - Assignment 1 Report}
\author{Tate Kennington - 5925152}
\date{}

\begin{document}
	\maketitle
	\section{Introduction}
	In recent years, neural networks have dominated much of the discourse surrounding machine learning, and yet, there is still relatively few concrete principles on how to build or use a neural network. Indeed, constructing effective neural networks relies heavily on the intuition of the engineer to make correct hyperparameter and architectural choices. In this report we look to quantify some of this intuition in the context of a simple multilayer feed forward network. Specifically, we investigate how changes to the networks basic hyper parameters impact the networks performance on a variety of tasks.
	\section{Background}
	The two hyperparameters we will be investigating in this report are the learning constant and the momentum constant. Specifically we want to measure how differing values of each of these constants affect the time to train for the network and the minimum error that the network can achieve.\\
	It is generally understood that learning constant controls the speed with which the network learns. In particular higher values cause the network to train faster at the cost of stability and accuracy. With network architectures and data sets becoming larger being able to improve training times is becoming increasingly important. For many applications a less accurate solution that can be trained quickly is preferable to the alternative. It is for this reason that we want to investigate the potentially training speed up gained by larger learning constants and what accuracy penalty, if any, this incurs.\\
	The addition of a momentum constant to a learning algorithm serves a similar purpose to the learning constant. Specifically it allows the network to accelerate it's rate of descent and potentially improve accuracy. However, it is not immediately clear what the "correct" momentum constant is for any given network, and incorrect choices may have a large impact on network performance. By having a better understanding of, roughly, how the momentum constant is related to network performance improves our ability to identify an optimal value, using a heuristic driven search for example. For this reason we have chosen to investigate the relationship between momentum constant and the network error rate and time to train.
	\section{The Impact of Learning Rate}
	\subsection{Method}
	To test the effect of different learning constants the network was trained on the popular "iris" data set until the network converged and training was stopped. Here we considered the network to have converged if the value of the population error had not changed by a value greater than 0.0001 for more than 1000 epochs. All tests were run on the "Hex" computer and all parameters besides the learning constant were kept fixed. Specifically the momentum constant was set to 0.9 and the network architecture consisted of 4 input neurons, 10 hidden neurons, and 3 output neurons. The results present are an average taken over 50 runs per value of the learning constant.
	\subsection{Results}
	Figures 1, 2, and 3 depict the results of these experiments. The quantities measured are the final population error, the number of epochs for the model to converge, and the number of epochs for the model to reach below a specific error threshold 0.008. Error bars denote one standard deviation of the sample.
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{learning-error.png}
		\caption{Error of the model as Learning Rate changes}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{learning-convergence.png}
		\caption{Time to converge for the model as Learning Rate changes}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{learning-time.png}
		\caption{Time to train for the model as Learning Rate changes}
	\end{figure}
	\newpage
	\subsection{Discussion}
	From our results we see that choice of learning rate has no substantive effect on model accuracy. We would expect this to be the case as the learning constant does not change how weights are updated only the magnitude of each update. That is, the model is still exploring the same error surface in the same way. Indeed it would seem that the model is finding the same error minima consistently regardless of learning constant. \\
	However, changing values of the learning constant do impact the models speed of learning and convergence, as one would expect. We see both the training time for a specific error threshold and convergence decreases rapidly with increasing learning constant, for small learning constant values. Increasing the learning constant past this point gives diminishing returns until the training time plateaus in both cases. At this point increasing the learning constant yields no further benefit to training time for a specific error threshold and causes the convergence time to begin increasing again. The increased time to converge for large error values would suggest that the larger step size causes the model to more frequently jump over, or out of, error minima. \\
	Taking these results together we can conclude that an optimal learning constant is one which is only sufficiently large to minimize the training time for a specific threshold. Any learning constant larger than the would seem to have no effect on the training time or model accuracy, whilst reducing model stability.
	\section{The Impact of Momentum Constant}
	\subsection{Method}
	Like with the learning constant, the effect of different momentums constants were also tested on the "iris" data set until the network converged and training was stopped. Again the network was considered to have converged if the value of the population error had not changed by a value greater than 0.0001 for more than 1000 epochs. All tests were run on the "Hex" computer and all parameters besides the learning constant were kept fixed. Specifically the the learning constant was set to 0.05 and the network architecture consisted of 4 input neurons, 10 hidden neurons, and 3 output neurons. The results present are an average taken over 50 runs per value of the momentum constant.
	\subsection{Results}
	The results of these experiments are depicted in Figures 4, 5, and 6. The quantities measured are the final population error, the number of epochs for the model to converge, and the number of epochs for the model to reach below a specific error threshold of 0.008. Error bars denote one standard deviation of the sample.
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{momentum-error.png}
		\caption{Error of the model as Momentum Constant changes}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{momentum-convergence.png}
		\caption{Time to converge for the model as Momentum Constant changes}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{momentum-time.png}
		\caption{Time to train for the model as Momentum Constant changes}
	\end{figure}
	\subsection{Discussion}
	First examining the time taken to train to a specific error threshold it would seem that changes in momentum constant have a largely negligible effect. As momentum constant increases the number of epochs taken on average only increases slightly. However, the variability of the number of epochs taken increases greatly with increasing momentum constant. This suggest that large values of the momentum constant reduce model stability. Comparing with the number of epochs taken to converge, we do see an increasing trend, which further supports decreased model stability. As with increasing learning constant, the likely cause of this is that larger momentum constants result in larger step sizes which in turn causes instability. \\
	On the other hand, momentum constant seems to have a strong effect on model accuracy. As the momentum constant increases the final error of the model decreases significantly. This is an interesting result as we hypothesized that the momentum constant would have little effect on model accuracy compared to the time to train. The fact that the momentum constant decreases model error indicates that different values of the momentum constant enable the model to converge to different error minima. It would follow then that the addition of momentum allows the model to escape from certain lcoal minima and, ultimately, converge to more optimal minima. \\
	With this in mind, we conclude that the choice of momentum constant depends on the individual task. If the only concern training speed then it is better to use little momentum. However, if high accuracy is important than a large momentum constant would be more suitable.
	\section{Discussion}
	To summarise our findings, it would seem that the common intuition dictating the use of a learning constant is correct. Larger values of a learning constant increase training speed, but introduce instability. Also increasing the learning rate only increases speed up to a point. However, the effects of the momentum constant are somewhat more nuanced, with increasing momentum constants improving accuracy but reducing training speed. This result runs counter to the authors intuitive understanding of the momentum mechanism. \\ 
	Future work into this topic includes investigating how these results change with different problem sets. This would allow us to better determine if the behaviour we've observed a quirk of the data set we used, or a more general characteristic of the parameters in question. Additionally we would like to see investigation into the interactions between pairs of changing parameters. For the purposes of this investigation we assumed that the interaction between parameter values did not effect their general behaviour. Finally, the effects of these parameters on model generalisation should be investigate. In our investigation we did not consider the effects of the parameters on generalisation. As such, it is possible that our results only hold when considering performance on the training set. \\
	To conclude, hyperparameter optimisation in neural networks is a very complex problem. Even in our small investigation we were unable to determine many hard and fast rules for parameter value settings, or strong conclusions as to the overall effect of a given parameter value setting. It would seem then, that neural network engineers are left with intuition and hyperparameter search as their two best options for constructing neural networks.
	\section{Appendix}
	\subsection{Program Overview}
	The program implements a a fully connected, feedforward network with exactly one hidden layer, using the generalised delta rule for training. Input tuples are read from a file and the network is trained against test outputs also read from a file. During training patterns are presented in a random order. Training proceeds until the network population error, calculated using mean squared error, drops below a configurable threshold. Alternatively the network can be configured to detect when it has converged, ie become stuck in a minima, and restart training in order to hopefully find a different minima which satisfies the error threshold. In this case, the network will keep restarting training up to a predefined maximum before simply stopping training. The requirements for determining convergence are configurable by the user. Neurons all use a sigmoid activation function, and activations which differ from 0 or 1 by a predefined epsilon are rounded down or up, respectively. In addition to the generalised delta rule weight changes are calculated also using a momentum component. The values of the learning constant and momentum constant are configurable by the user. The initial connection weight are chosen at random uniformly in the interval $[-0.3,0.3]$.
	\subsection{Compiling and Running}
	To compile the program, ensure that you're in the directory containing Main.java and NeuralNetwork.java, and run the command,
	\begin{verbatim}
	javac *.java
	\end{verbatim}
	 Invoke the program using,
	\begin{verbatim}
	java Main
	\end{verbatim}
	The program will read input values, training values, and parameters from the in.txt, teach.txt and param.txt files located in the program directory.
	\subsection{Input and Parameter files}
	\paragraph{in.txt} 
	~\\The in.txt file contains the input vectors, one on each line. Values are real numbers and should be separated by a single space. Every vector should have the same number of elements and the number of elements should match the number of input neurons defined in the param.txt file.
	\paragraph{teach.txt}
	~\\The teach.txt file contains the teaching vectors, one on each line. Values are real numbers and should be separated by a single space. Every vector should have the same number of elements and the number of elements should match the number of output neurons defined in the param.txt file.
	\paragraph{param.txt}
	~\\The param.txt file contains the configurable parameters of the network. The parameters that are available for configuration are listed below.
	\begin{itemize}
		\item Input : The number of neurons in the input layer, should be a positive integer.
		\item Hidden : The number of neurons in the hidden layer, should be a positive integer. 
		\item Output : The number of neurons in the output layer, should be a positive integer.
		\item Learning : The learning constant used to calculate weight change, should be a positive real number.
		\item Momentum : The momentum constant used to calculate weight change, should be a positive real number.
		\item Error Criteria : The minimum acceptable error value, the networks stops training once population error reaches this value, should be a non-negative real number.
		\item Batch Size : Number of examples presented to the network in a single batch, should be a positive integer.
		\item Epoch Interval : Number of epochs between diagnostic outputs, should be a positive integer.
		\item Convergence Threshold : The threshold for which error values are considered different for the purpose of determining convergence, should be a non-negative real number, values of 0 disable early stopping.
		\item Convergence Epochs : The number of epochs the network must have stayed in a minima before it is considered to have converged, should be a positive integer.
	\end{itemize}
	The format of the param.txt file is a single value on each line corrosponding to each of the above parameters, in the same order as presented above. The first six of these parameters are compulsory, whereas the last four are optional. However, while the last four parameters are optional they must all be included together.
	\subsection{Command line flags}
	The program also allows for a number of parameters to be controlled via command line flags. The flags are,
	\begin{itemize}
		\item -l [value] : This sets the learning constant to the given value, the given value should be a positive real number.
		\item -m [value] : This sets the momentum constant to the given value, the given value should be a positive real number.
		\item -t : Runs the network on the population of input patterns and displays network output and activation for each pattern. 
		\item -w : Displays the network weights in the format, "input, output, weight".
	\end{itemize}
	The values for parameters given as command line flags will supercede any parameter settings in the param.txt file. For example if the "-l 0.5" flag is passed to the program the learning constant will be 0.5 regardless of the contents of param.txt. This is to allow for increased automation when testing different parameter settings.
\end{document}